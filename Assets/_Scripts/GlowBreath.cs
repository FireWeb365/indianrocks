﻿using UnityEngine;
using System.Collections;

public class GlowBreath : MonoBehaviour
{
	public float Speed = 3f;
	public float TargetA = 0.7f;
	public float TargetB = 1.3f;
	Renderer renderer;
	float BreathVal;
	float BreathTarget;
	bool CurInt;

	void Awake()
	{
		renderer = GetComponent<Renderer>();

		BreathTarget = TargetB;
		CurInt = true;
	}
	void Update()
	{
		if(BreathTarget == TargetB && BreathVal > TargetB - 0.1f)
		{
			BreathTarget = TargetA;
			CurInt = false;
		}
		else if(BreathTarget == TargetA && BreathVal < TargetA + 0.1f)
		{
			BreathTarget = TargetB;
			CurInt = true;
		}
		BreathVal = Mathf.SmoothStep(BreathVal, BreathTarget, Time.deltaTime * Speed);

		renderer.material.SetFloat("_MKGlowPower", BreathVal);
	}
}