﻿using UnityEngine;
using System.Collections;

public class GetBackCol : MonoBehaviour
{
	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.tag == "Player")
		{
			FindObjectOfType<BetBackSys>().Enters++;
		}
	}
	void OnTriggerExit2D(Collider2D col)
	{
		if(col.tag == "Player")
		{
			FindObjectOfType<BetBackSys>().Enters--;
		}
	}
}