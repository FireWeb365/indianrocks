﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BetBackSys : MonoBehaviour
{
	public Text CDText;
	public float Countdown;
	[HideInInspector]
	public int Enters;
	float CD;

	void Update()
	{
		if(Enters > 0)
		{
			CD -= Time.deltaTime;

			CDText.text = (Mathf.RoundToInt(CD * 10) / 10f).ToString();

			CDText.enabled = true;

			if(CD < 0)
			{
				Application.LoadLevel(Application.loadedLevel);
			}
		}
		else
		{
			CD = Countdown;

			CDText.enabled = false;
		}
	}
}