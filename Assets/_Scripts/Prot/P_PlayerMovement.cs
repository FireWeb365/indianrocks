﻿using UnityEngine;
using System.Collections;

public class P_PlayerMovement : MonoBehaviour
{
	public bool Look = true;
	public float walkSpeed = 10f;
	private float curSpeed;
	Rigidbody2D RB;

	void Awake()
	{
		RB = GetComponent<Rigidbody2D>();
	}
	void Update()
	{
		curSpeed = walkSpeed;

		Vector2 NewVel;

		NewVel = new Vector2(Mathf.Lerp(0, Input.GetAxis("Horizontal") * curSpeed, 0.8f), Mathf.Lerp(0, Input.GetAxis("Vertical") * curSpeed, 0.8f));
		RB.velocity = NewVel;

		if(Look)
		{
			float camDis = Camera.main.transform.position.y - transform.position.y;
			Vector3 mouse = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, camDis));
			float AngleRad = Mathf.Atan2 (mouse.y - transform.position.y, mouse.x - transform.position.x);
			float angle = (180 / Mathf.PI) * AngleRad;
			
			RB.rotation = angle - 90f;
		}
	}
}