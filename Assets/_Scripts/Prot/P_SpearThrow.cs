﻿using UnityEngine;
using System.Collections;

public class P_SpearThrow : MonoBehaviour
{
	public bool HaveSpear;
	public Rigidbody2D SpearObject;
	public Vector3 NormalSpear;
	public Vector3 ReadySpear;
	public float ThrowSpeed = 100f;
	public float BackSpeed = 40f;
	float ForceMultp;
	bool Charging;
	BoxCollider2D Coll;
	bool Getting;

	void Awake()
	{
		Coll = GetComponent<BoxCollider2D>();
	}
	void Update()
	{
		//Colliders truth
		if(HaveSpear)
		{
			Coll.enabled = false;

			SpearObject.transform.localRotation = Quaternion.Euler(Vector3.forward * 90f);
		}
		//Getting solving
		if(Getting && !Input.GetMouseButton(0))
		{
			Getting = false;
		}
		//MouseInput
		if(Input.GetMouseButton(0) && HaveSpear && !Getting)
		{
			Charging = true;

			SpearObject.transform.localPosition = Vector3.Lerp(SpearObject.transform.localPosition, ReadySpear, Time.deltaTime * 1f);

			ForceMultp = Mathf.Lerp(ForceMultp, 1f, Time.deltaTime * 1.5f);
		}
		else if(Input.GetMouseButton(0) && !HaveSpear)
		{
			SpearObject.AddForce((transform.position - SpearObject.transform.position).normalized * BackSpeed);
		}
		else if(HaveSpear && !Charging)
		{
			ForceMultp = 0f;

			SpearObject.transform.localPosition = Vector3.Lerp(SpearObject.transform.localPosition, NormalSpear, Time.deltaTime * BackSpeed);
		}
		else
		{
			if(Charging && HaveSpear)
			{
				HaveSpear = false;
				
				Charging = false;
				
				SpearObject.isKinematic = false;
				
				SpearObject.AddRelativeForce(Vector2.right * ThrowSpeed * ForceMultp, ForceMode2D.Impulse);
				
				SpearObject.gameObject.transform.parent = null;

				StartCoroutine(TurnOnRetrievingColl());
			}
		}
	}
	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.gameObject == SpearObject.gameObject)
		{
			Getting = true;
			HaveSpear = true;
			
			SpearObject.isKinematic = true;

			SpearObject.transform.parent = transform;

			//SpearObject.transform.localPosition = NormalSpear;
		}
	}
	IEnumerator TurnOnRetrievingColl()
	{
		yield return new WaitForSeconds(0.3f);

		Coll.enabled = true;
	}
}